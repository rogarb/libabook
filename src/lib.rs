pub mod database;
pub mod entry;

#[cfg(test)]
mod tests {
    #[test]
    fn parse_abook_file() {
        //let file = "/home/romain/.abook/addressbook";
        let file = "tests/abook.test";
        let _database = crate::database::Database::from_file(file);
    }
    
    #[test]
    fn test_vcard_from_entry() {
        let file = "tests/abook.test";
        let database = crate::database::Database::from_file(file);

        for entry in database.entries() {
            println!("{}", entry.to_abook());
        }
    }

    #[test]
    fn test_vcard_from_database() {
        let file = "tests/abook.test";
        let database = crate::database::Database::from_file(file);

        println!("{}", database.to_vcard());
        assert!(false);
    }
}
