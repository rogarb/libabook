/// Representation of an entry in the database

use email_address_parser::EmailAddress;
use vcard::VCard;
use std::collections::HashSet;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Entry {
    id: usize,
    name: Option<String>,
    emails: Option<Vec<EmailAddress>>, // initially using a hashset for this, which cannot be used in a super hashset, so using Vec
                                       // TODO: implement PartialEq for Entry so ordering doesn't
                                       // matter
    phone: Option<String>,  // TODO: use crate phonenumber to represent phone numbers
    workphone: Option<String>,
    mobile: Option<String>,
    // TODO: implement more fields
}

impl Entry {
    pub fn new(id: usize) -> Entry {
        Entry {
            id: id,
            name: None,
            emails: None,
            phone: None,
            workphone: None,
            mobile: None,
        }
    }

    pub fn set_name(&mut self, name: &str) {
        self.name = Some(name.to_string());
    }

    pub fn set_workphone(&mut self, workphone: &str) {
        self.workphone = Some(workphone.to_string());
    }

    pub fn set_mobile(&mut self, mobile: &str) {
        self.mobile = Some(mobile.to_string());
    }

    pub fn set_phone(&mut self, phone: &str) {
        self.phone = Some(phone.to_string());
    }

    pub fn add_email(&mut self, emails: &str) {
        for email in emails.split(',') {
            if let Some(email) = EmailAddress::parse(email, None) {
                if self.emails.is_none() {
                    self.emails = Some(Vec::new());
                }
                self.emails.as_mut().unwrap().push(email);
            }
        }
                    
    }

    pub fn to_vcard(&self) -> VCard {
        // TODO: properly handle errors
        assert!(self.name.is_some(), "Unable to convert Entry to VCard: no field 'name' present");
       
        let mut vcard = VCard::from_formatted_name_str(self.name.clone().unwrap().as_str()).unwrap();

        if let Some(emails) = self.emails.clone() {
            let mut email_set = HashSet::new();
            for email in emails {
                // for debugging purposes
                match vcard::values::email_value::EmailValue::from_string(format!("{}", email)) {
                    Ok(e) => if email_set.insert(vcard::properties::Email::from_email_value(e.clone())) == false { println!("DBG: value '{}' already exists, ignoring", e) },
                    //Err(e) => panic!("Error: {} (value: {})", e, email),
                    Err(e) => println!("Error: {} (value: {}), discarding", e, email),
                }
            }
            if email_set.len() == 0 {
                vcard.emails = None; 
            } else {
                vcard.emails = Some(vcard::Set::from_hash_set(email_set).unwrap());
            }
        }

        let mut phones = HashSet::new();
        if let Some(phone) = self.phone.clone() {
            phones.insert(vcard::properties::Telephone::from_text(
                    vcard::values::text::Text::from_string(phone).unwrap()));
        }

        if let Some(workphone) = self.workphone.clone() {
            phones.insert(vcard::properties::Telephone::from_text(
                    vcard::values::text::Text::from_string(workphone).unwrap()));
        }

        if let Some(mobile) = self.mobile.clone() {
            phones.insert(vcard::properties::Telephone::from_text(
                    vcard::values::text::Text::from_string(mobile).unwrap()));
        }

        if ! phones.is_empty() {
            vcard.telephones = Some(vcard::Set::from_hash_set(phones).unwrap());
        }

        vcard
    }

    /// Generate a String compliant to abook format
    pub fn to_abook(&self) -> String {
        let mut entry = format!("\n[{}]\n", self.id);
        if let Some(name) = self.name.clone() {
            entry.push_str(format!("name={}\n", name).as_str());
        }
        if let Some(emails) = self.emails.clone() {
            let emails = {
                let mut s = String::new();
                for email in emails {
                    s.push_str(format!("{},", email).as_str());
                }
                s
            };
            entry.push_str(format!("email={}\n", emails.as_str().trim_end_matches(',')).as_str());
        }
        if let Some(phone) = self.phone.clone() {
            entry.push_str(format!("phone={}\n", phone).as_str());
        }
        if let Some(workphone) = self.workphone.clone() {
            entry.push_str(format!("workphone={}\n", workphone).as_str());
        }
        entry.push('\n');
        entry
    }
}
