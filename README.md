libabook: library for manipulating abook address book files
===========================================================

Features:
---
* Extract the entries from an abook file
* Write the address book to an arbitrary file
* Supports `name`, `email`, `phone` and `workphone` fields (TODO: support more fields)
* Database or separate entries can be exported to vcard format (as a String representation)
* TODO: merge address books
